# Documentation

## Domain Diagram
![Domain Diagram](classdiagram.png)	
	
## Entities
	
| Entity  | Description | 
|------------- | -------------| 
| Base | - |
| Project | Information about Project|
| Component | Group of user presents in a Project|
| User | Information about User|
| Group | Group of users|
| Avatar | - |
| Role | Roles present in a project|
| Issue | - |
| Status | - |
| StatusCategory | - |
| Priority | Issue Priority|
| Resolution | - |
| Comment | - |
| Attachment | - |
| Dashboard | - |

## Entities Details
### Base
| Function  | Description| 
| ------------- | -------------| 
| find_all | - |
| find | - |
### User
| Function  | Description| 
| ------------- | -------------| 
| find | - |
### Dashboard
| Function  | Description| 
| ------------- | -------------| 
| find_all | - |
| find | - |
		
## Copyright
This lib was PowerRight by [SEON Application Lib Generator](https://gitlab.com/mdd_seon/application) 
