from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='jiraX',  # Required
    version='0.0.1',  # Required
    author="Paulo Sérgio dos Santos Júnior",
    author_email="paulossjunior@gmail.com",
    description="Lib created based on documentaion https://docs.atlassian.com/DAC/rest/jira/6.1.html",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/integration_seon/libs/application/jira",
    packages=find_packages(),
    
    install_requires=[
        'jira==2.0.0', 'factory-boy==2.12.0',
    ],

    classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
    setup_requires=['wheel'],
    
)


