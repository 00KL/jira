from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

issues = None
issue_apl = None
sprint = None


@given(u'a project and a sprint')
def step_impl(context):
    global issue_apl
    global sprint
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    issue_apl = factory.IssueFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_project("STH") 
    board = project_apl.find_board(project.key)[0]
    sprint = project_apl.find_sprint(board.id)[0]
    

@when(u'i request all sprint\'s issues of a project to API')
def step_impl(context):
    global issues
    issues = issue_apl.find_by_sprint(sprint.id)


@then(u'i get a list of issues')
def step_impl(context):
    assert issues != []