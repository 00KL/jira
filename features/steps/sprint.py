from behave import given, when, then

from jira import JIRA
from pprint import pprint
from jiraX import factories as factory

project_apl = None
project = None
sprints = None

@given(u'a project')
def given_a_project(context):
    global project_apl
    global project
    user = "lucasmoraesplay@gmail.com"
    apikey = "seApnbFfBXp6AVCdanCK8DFB"
    server =  'https://ledszeppellin.atlassian.net/' #Site = organização
    project_apl = factory.ProjectFactory(user=user,apikey=apikey,server=server)
    #Stairway to heaven project
    project = project_apl.find_project("STH") 


@when(u'i request all sprints of a project to API')
def step_impl(context):
    global sprints
    boards = project_apl.find_board(project.key)
    sprints = [sprint for board in boards for sprint in project_apl.find_sprint(board.id)]


@then(u'i get a list of sprints')
def step_impl(context):
    assert sprints != []

