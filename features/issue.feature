# Eu, como desenvolvedor, quero recuperar 
# as dados das tarefas de um projeto dentro de um sprint.
Feature: Retrive all issues from sprint of a project

    Scenario: get all issues
    Given a project and a sprint
    When i request all sprint's issues of a project to API
    Then i get a list of issues